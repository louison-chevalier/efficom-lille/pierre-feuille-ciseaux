Pierre, Feuille, Ciseaux
==========

Le jeu est très simple :   
    -  la pierre gagne contre les ciseaux    
    -  les ciseaux gagnent sur la feuille    
    -  la feuille gagne sur la pierre    

Donc on choisit un des trois et l'adversaire aussi et on le fait voir si le choix est identique, le tour ne compte pas sinon le gagnant à 1 point. La manche va jusqu'à 10 points.